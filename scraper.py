import urllib.request as urllib2
import requests
import json
from bs4 import BeautifulSoup
import geocoder
from geopy.geocoders import Nominatim
import requests
from haversine import haversine

def scrapey(urlthing):
    locator = Nominatim(user_agent = "myGeocoder")

    ee_dict = {"aluminium": 6.0, "clay": 0.3, "ceramics": 0.7, "glass": 1.4, "cork": 0.2, "copper": 2.7, 
            "iron": 2.0, "lead": 1.7, "cotton": 4.0, "silver": 6.3, "paint": 2.9, "paper": 1.4, "plaster": 0.1, 
            "general plastic": 3.3, "polyethylene": 2.5, "ABS": 3.8, "nylon": 7.9, "polycarbonate": 7.6,
            "polypropylene": 3.4, "polystyrene": 3.4, "polyurethane": 4.8, "PVC": 3.1, "Rubber": 2.9, 
            "steel": 2.8, "rebar": 2.0, "general stone": 0.1, "general timber": 0.5, "tin": 3.0,
           "titanium": 25.0}

    ee_list = []
    wordurl = urlthing[27:]
    wordurl = wordurl.split('/')[0]
    urllist = wordurl.split('-')
    fwordurl = ""
    for bants in urllist:
        fwordurl = fwordurl + bants + ' '

    soup = BeautifulSoup(urllib2.urlopen(urlthing).read())
    page = soup.get_text()
    bulk = []
    matlist = []
    matdict = []
    mat_list = []

    bulk = page.split()
    placehold = []
    weighthold = []

    for item in ee_dict:
        ee_list.append(item)

    for n in range(len(bulk)):
        if (bulk[n] == "Item"):
            if (bulk[n+1] == "location:"):
                placehold = bulk[n+2:n+5]
            else:
                pass
        else:
            pass

        if ((bulk[n] == "Weight:") or (bulk[n] == "Weight") or (bulk[n] == "weight") or (bulk[n] == "weight:")):
                weighthold.append(bulk[n+1])
        else:
            pass

        for p in range (len(ee_list)):
            if (bulk[n] == ee_list[p]):
                matlist.append(ee_list[p])
            else:
                pass

    matdict = dict.fromkeys(matlist)
    for thing in matdict:
        mat_list.append(thing)

    place = placehold[0] + " " + placehold[1] + " " + placehold[2]
    weight = weighthold[0]
    if 'g' in weight:
        weight = weight[:-1]
    else:
        weight = weight

    if 'K' in weight:
        weight = weight[:-1]
    else:
        weight = weight

    if 'k' in weight:
        weight = weight[:-1]
    else:
        weight = weight
    weightnum = float(weight)

    recieverarray = geocoder.ipinfo('me')
    reciever = recieverarray.latlng
    sender = ((locator.geocode(place)).latitude, (locator.geocode(place)).longitude)
    dist = haversine(sender, reciever)
    cotwo = dist * 0.286 * weightnum
    
    return [dist, cotwo, weightnum, place, mat_list, fwordurl]

