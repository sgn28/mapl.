const GENER8_BACKEND_URL = 'https://api.gener8ads.com/';
const GENER8_FRONTEND_URL = 'https://user.gener8ads.com/';
const GENER8_WEBSITE = 'https://gener8ads.com';
const SCHEDULER_DELAY_MIN = 60 * 6; // 60 * hours
const GENER8_POPUP_DISABLE_HOURS = 24 * 7; // hours
const GENER8_EXT_URL = 'https://chrome.google.com/webstore/detail/gener8/agplbamogoflhammoccnkehefjgkcncl/reviews';

const GENER8_APOLLO_API = 'https://apollo.gener8ads.com';
const GENER8_APOLLO_CLIENT_ID = '1f3c83f8-50e4-463b-84f6-9cfb6382da0d';
