/* global getUserAccessToken, ajaxCall, GENER8_BACKEND_URL, SCHEDULER, ADD_WHITELIST */

function ready(fn) {
    if (document.readyState !== 'loading') {
      fn();
    } else {
      document.addEventListener('DOMContentLoaded', fn);
    }
  }
  
  // This is checking the domain removing it from gener8 to inject ads into
  
  function whitelist(key, domainName, token, enable, callback) {
    if (!domainName || !domainName.length) {
      return;
    }
  
    const body = {
      enable,
      type: key,
      domainName,
    };
    ajaxCall('POST', 'application/json', ADD_WHITELIST, body, 'JSON', token, callback);
  }
  
  function extractHostname(url) {
    if (!url) return '';
    // find & remove protocol (http, ftp, etc.) and get hostname
    let hostname = url.indexOf('://') > -1 ? url.split('/')[2] : url.split('/')[0];
  
    // find & remove port number
    [hostname] = hostname.split(':');
  
    // find & remove "?"
    [hostname] = hostname.split('?');
  
    return hostname;
  }
  
  function extractLink(url) {
    if (!url) {
      return '';
    } return url.split('?')[0];
  }
  
  function updateStorage(type, enable, hostName) {
    chrome.storage.local.get([type], (local) => {
      if (enable) {
        if (local[type]) {
          local[type].push(hostName);
        } else {
          local[type] = [hostName];
        }
      } else if (local[type]) {
        local[type].splice(local[type].indexOf(hostName), 1);
      } else {
        local[type] = [];
      }
      chrome.storage.local.set({
        [type]: local[type],
      });
    });
  }
  
  function updateDashboard(data, domainName, pageName) {
    const { adminWhitelist, userWhitelist, pageWhitelist } = data;
    const isAdminWhitelisted = adminWhitelist && adminWhitelist.includes(domainName);
    const isWhitelist = userWhitelist && userWhitelist.includes(domainName);
    const isPageWhitelist = pageWhitelist && pageWhitelist.includes(pageName);
  
    const $pause = document.querySelector('#pause-toggle');
    $pause.disabled = isAdminWhitelisted || isWhitelist;
    $pause.checked = isPageWhitelist;
  
    const $stop = document.querySelector('#stop-toggle');
    $stop.checked = isWhitelist;
    $stop.disabled = isAdminWhitelisted;
  
    const $balance = document.querySelector('.wallet__balance-amount');
    $balance.innerText = parseFloat(data.user.walletToken).toFixed(2);
  
    const $referralLink = document.querySelector('.refer__link');
    $referralLink.value = data.user.referralLink;
  
    if (isAdminWhitelisted || isWhitelist || isPageWhitelist) {
      const $body = document.querySelector('body');
      $body.classList.add('state--paused');
    }
  }
  
  function requestError(error) {
    // generExtBody.empty();
    switch (error.status) {
      case 423:
        // generExtBody.append(suspendPage('Account Suspended', error.responseJSON.message));
        chrome.runtime.sendMessage({ action: 'deleteToken' });
        break;
      case 503:
        // generExtBody.append(suspendPage('We\'ll back soon!', error.responseJSON.message));
        break;
      case 401:
        chrome.runtime.sendMessage({ action: 'deleteToken' });
        // generExtBody.append(loginPage);
        break;
      case 451: {
        chrome.runtime.sendMessage({ action: 'SET_TNC', data: error.responseJSON.data.tnc.version });
        const message = `We have updated the new T&C,
          please accept it to continue.
          You can read the new T&C <a href='#' id='tnc'>here</a>
          <button class="g8-tnc" id='accept-tnc'>Accept</button>
        `;
        // generExtBody.append(suspendPage('Please accept T&C', message));
        break;
      } default:
        // generExtBody.append(loginPage);
        break;
    }
  }
  
  function schedulerAPI(token, domainName, pageName, cb) {
    $.ajax({
      url: GENER8_BACKEND_URL + SCHEDULER,
      method: 'GET',
      dataType: 'json',
      crossDomain: true,
      contentType: 'application/json; charset=utf-8',
      beforeSend(xhr) {
        xhr.setRequestHeader('Authorization', token);
      },
      success(success) {
        const userData = success.data;
        userData.user.walletToken = parseFloat(userData.user.walletToken);
        chrome.storage.local.set({
          isGener8On: userData.isGener8On,
          pageWhitelist: userData.pageWhitelist,
          userWhitelist: userData.userWhitelist,
          token,
          user: userData.user,
          adminWhitelist: userData.adminWhitelist,
          userStatusCode: null,
          errorMessage: '',
        });
  
        const adTags = {};
        success.data.adtags.forEach((tag) => {
          adTags[`${tag.width}x${tag.height}`] = tag.content;
        });
        userData.user.tokenRate = success.data.tokenRate;
        chrome.runtime.sendMessage({ action: 'SET_USERDATA', data: userData.user, adTags });
        chrome.runtime.sendMessage({
          action: 'FRAUD_PREVENTION',
          data: userData,
        });
        // generExtBody.empty();
        updateDashboard(userData, domainName, pageName);
        if (cb) cb(userData);
      },
      error(error) {
        let errorMessage;
  
        if (error.responseJSON && error.responseJSON.message) {
          errorMessage = error.responseJSON.message;
        }
  
        chrome.storage.local.set({
          userStatusCode: error.status,
          isGener8On: null,
          pageWhitelist: null,
          userWhitelist: null,
          token,
          user: null,
          adminWhitelist: null,
          errorMessage,
        });
        requestError(error);
      },
    });
  }
  
  function getUserDetails(token, domainName, pageName, cb) {
    const localStorageKeys = ['token', 'isGener8On', 'pageWhitelist', 'userWhitelist', 'adminWhitelist', 'user', 'userStatusCode', 'notificationCount', 'errorMessage'];
    chrome.storage.local.get(localStorageKeys, (tokenData) => {
      const currentToken = tokenData.token;
      if (currentToken !== token) {
        schedulerAPI(token, domainName, pageName, cb);
      } else {
        // generExtBody.empty();
        switch (tokenData.userStatusCode) {
          case 423:
            // generExtBody.append(suspendPage('Account Suspended', tokenData.errorMessage ? tokenData.errorMessage : null));
            chrome.runtime.sendMessage({ action: 'deleteToken' });
            break;
          case 503:
            // generExtBody.append(suspendPage('We\'ll back soon!', tokenData.errorMessage ? tokenData.errorMessage : null));
            break;
          case 451:
            cookieGet('tncAccepted', (tnc) => {
              // check tnc accepted by user paner
              if (tnc && JSON.parse(tnc).body) {
                schedulerAPI(token, domainName, pageName, cb);
              } else {
                const message = `We have updated the new T&C,
                                please accept it to continue.
                                You can read the new T&C <a href='#' id='tnc'>here</a>
                                <button class="g8-tnc" id='accept-tnc'>Accept</button>
                                `;
                // generExtBody.append(suspendPage('Please accept T&C', message));
              }
            });
            break;
          default:
            updateDashboard(tokenData, domainName, pageName);
            break;
        }
      }
    });
  }
  
  ready(() => {
    const $body = document.querySelector('body');
    let token;
    const pauseClass = 'state--paused';
  
    getUserAccessToken((t) => {
      token = t;
      $body.classList.remove('state--loading');
      if (token) {
        $body.classList.add('state--dashboard');
        chrome.tabs.query({ currentWindow: true, active: true }, ([tab]) => {
          getUserDetails(token, extractHostname(tab.url), extractLink(tab.url));
        });
      } else {
        $body.classList.add('state--login');
      }
    });
  
    const $video = document.querySelector('#video');
    const $pauseButton = document.querySelector('.pause-toggle');
    const $offButton = document.querySelector('.stop-toggle');
    const $copyLink = document.querySelector('#copy-link');
    const $slider = document.querySelector('#slider');
  
    $video.addEventListener('loadedmetadata', function () {
      this.currentTime = 2;
    }, false);
  
    $pauseButton.addEventListener('change', () => {
      const enable = $pauseButton.checked;
  
      if (enable) {
        $body.classList.add(pauseClass);
      } else {
        $body.classList.remove(pauseClass);
      }
  
      chrome.tabs.query({ currentWindow: true, active: true }, ([tab]) => {
        const hostName = extractLink(tab.url);
        whitelist('page', hostName, token, enable, () => {
          updateStorage('pageWhitelist', enable, hostName);
          chrome.tabs.reload(tab.id);
        });
      });
    });
  
    const change = () => {
      const enable = $offButton.checked;
  
      if (enable) {
        $body.classList.add(pauseClass);
      } else {
        $body.classList.remove(pauseClass);
      }
  
      chrome.tabs.query({ currentWindow: true, active: true }, ([tab]) => {
        const hostName = extractHostname(tab.url);
        whitelist('domain', hostName, token, enable, () => {
          updateStorage('userWhitelist', enable, hostName);
          chrome.tabs.reload(tab.id);
        });
      });
    };
  
    $offButton.addEventListener('change', change);
  
    $slider.addEventListener('click', () => {
      $offButton.checked = !$offButton.checked;
      change();
    });
  
    $copyLink.addEventListener('click', () => {
      const $input = document.querySelector('#link-input');
      $input.select();
      document.execCommand('copy');
    });
  });
  