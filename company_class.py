class Company:
    # A class to represent the raw data for each company in python
    def __init__(self, company_name, company_type, c_footprint, renewable_percent,
                      freshwater_usage):
        self.company_name = company_name
        self.company_type = company_type #e.g retail
        self.c_footprint = c_footprint #in Mtn/yr
        self.renewable_percent = renewable_percent #% of energy gen. by renewables
        self.freshwater_usage = freshwater_usage #in kl/yr
    
    def test(self, company_name, company_type, c_footprint, renewable_percent,
                      freshwater_usage):
        try:
            assert type(company_name) == str
        except AssertionError:
            company_name = str('<empty>')

        try:
            assert type(company_type) == str
        except AssertionError:
            company_type = str('<empty>')

        try:
            assert type(c_footprint) == float or type(c_footprint) == int
        except AssertionError:
            c_footprint = str('<empty>')


        try:
            assert type(renewable_percent) == float or type(renewable_percent) == int
        except AssertionError:
            company_type = str('<empty>')

        try:
            assert type(freshwater_usage) == float or type(freshwater_usage) == int
        except AssertionError:
            company_type = str('<empty>')

    
    def __repr__(self):
        c = "\n A full breakdown of environmental impact for {}".format(self.company_name)
        c += "\nType: {}".format(self.company_type)
        c += "\nCarbon Footprint: {} MTonnes/yr".format(self.c_footprint)
        c += "\nEnergy sourced from renewables: {}".format(self.renewable_percent)
        c += "\nFreshwater usage: {} kl/year".format(self.freshwater_usage)
        return c
