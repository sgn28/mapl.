#sustainability_algorithm.py
#calculate the sustainability index for a product as defined in the class.py files
from product_class import *
from scraper import scrapey
from fetch_url import fetch_url_from_path
#Product(self, product_name, shipping_distance, embodied_emissions, recyclability, score)

def calculate_score():
    #fetch data from scrape
    scrape_data = scrapey(fetch_url_from_path())
    shipping_distance = round(scrape_data[0], 1)
    shipping_emissions = round(scrape_data[1], 1)
    weight = scrape_data[2]
    location = scrape_data[3]
    materials = []
    materials.append(scrape_data[4])
    product_name = scrape_data[5]
    
        #embodied emissions of each material in kgCO2/kg
    ee_dict = {"aluminium": 6.0, "clay": 0.3, "ceramics": 0.7, "glass": 1.4, "cork": 0.2, "copper": 2.7, 
            "iron": 2.0, "lead": 1.7, "cotton": 4.0, "silver": 6.3, "paint": 2.9, "paper": 1.4, "plaster": 0.1, 
            "general plastic": 3.3, "polyethylene": 2.5, "ABS": 3.8, "nylon": 7.9, "polycarbonate": 7.6,
            "polypropylene": 3.4, "polystyrene": 3.4, "polyurethane": 4.8, "PVC": 3.1, "Rubber": 2.9, 
            "steel": 2.8, "rebar": 2.0, "general stone": 0.1, "general timber": 0.5, "tin": 3.0,
            "titanium": 25.0}

    rcy = {"aluminium": True, "clay": True, "ceramics": False, "glass": True, "cork": False, "copper": True, 
            "iron": True, "lead": True, "cotton": False , "silver": True, "paint": False, "paper": False, "plaster": False, 
            "general plastic": False, "polyethylene": True, "ABS": True, "nylon": False, "polycarbonate": True,
            "polypropylene": True, "polystyrene": False, "polyurethane": True, "PVC": False, "Rubber": True, 
            "steel": True, "rebar": True, "general stone": False, "general timber": True, "tin": True,
            "titanium": True}

    
    #calculate the percentage recyclability
    rcy_able_list = []
    true_count = 0 
    false_count = 0
    materials_new = []
    
    for i in materials:
        for j in i:
            materials_new.append(j)
    
    materials = materials_new
    
    for i in materials:
        for key in rcy:
            if i == key:
                rcy_able_list.append(rcy[key])
            else:
                continue

    for i in rcy_able_list:
        if i == True:
            true_count += 1
        else:
            false_count += 1
    recyclability = (true_count / (true_count + false_count)) * 100

    #calculate the embodied emissions
    emb_cost = 0

    for i in materials:
        if type(weight) == float:
            for key in ee_dict:
                if key.lower() == i.lower():
                    emb_cost += weight * ee_dict[key]
                else:
                    continue

        else:
            for i in materials:
                if key == i:
                    emb_cost += ee_dict[key]
                else:
                    continue
    embodied_emissions = round(emb_cost, 1)

    # calculating the final sustainability index, higher score = more sustainable
    # apply weighting to each factor

    score = (1 / (embodied_emissions + (1.1 * shipping_emissions))) * recyclability

    #classifying the score into star ratings - these are arbitrary for now
    #and should be adjusted when typical range data becomes available
    one_star = 0.1
    two_star = 0.2
    three_star = 0.3
    four_star = 0.4
    five_star = 0.5

    if score <= one_star:
        score = 0
    elif score > one_star and score <= two_star:
        score = 1
    elif score > two_star and score <= three_star:
        score = 2 
    elif score > three_star and score <= four_star:
        score = 3
    elif score > four_star and score <= five_star:
        score = 4
    elif score > five_star:
        score = 5

    #fix the product name
    product_name = product_name.replace("-", " ")
    
    Product_i = Product(product_name, shipping_emissions, embodied_emissions, recyclability, shipping_distance, location, score)

    #data dump to data.txt file
    data_path = "/Users/Home/Downloads/data.txt"
    with open(data_path, "w") as data_file:
        temp = str(Product_i)
        data_file.write(temp)

calculate_score()


