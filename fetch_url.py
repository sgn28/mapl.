# this code should read the url from the file that is outputted from the front end.
# the url is then scraped for data following the url retrieval

def fetch_url_from_path(path = "/Users/Home/Downloads/url.txt"):
    url = []
    with open(path, "rt") as url_file:
        #the following code assumes the url is on the top line of the file, 
        #and no other data is present on the top line.
        for line in url_file:
            url.append(line)

    return url[0]


