class Product:
    # A class for representing product information on online retailers' websites.
    def __init__(self, product_name, shipping_emissions, embodied_emissions, recyclability, shipping_distance, location, score):
        self.product_name = product_name
        self.shipping_emissions = shipping_emissions #in kilometres
        self.embodied_emissions = embodied_emissions #in kg CO2
        self.recyclability = recyclability
        self.score = score
        self.shipping_distance = shipping_distance
        self.location = location

    def __str__(self):
        p = "\nA full breakdown of environmental impact for {}".format(self.product_name)
        p += "\n\nShipping emissions: {} kgCO2".format(self.shipping_emissions)
        p += "\n\nEmbodied Emissions: {} kgCO2".format(self.embodied_emissions)
        p += "\n\nRecyclability %: {}".format(self.recyclability)
        p += "\n\nShipping distance: {} km".format(self.shipping_distance)
        p += "\n\nLocation: {}".format(self.location)
        p += "\n\nSustainability index: {}".format(self.score)

        return p
 
    def test(self):
        try:
            assert type(self.shipping_emissions) == float
        except AssertionError: 
            self.shipping_emissions = str('<empty>')
        try:
            assert type(self.embodied_emissions) == float
        except AssertionError: 
            self.embodied_emissions = str('<empty>')
        try:
            assert self.recyclability == True or self.recyclability == False
        except AssertionError: 
            self.recyclability = str('<empty>')


