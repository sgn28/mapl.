ee_dict = {"aluminium": 6.0, "clay": 0.3, "ceramics": 0.7, "glass": 1.4, "cork": 0.2, "copper": 2.7, 
        "iron": 2.0, "lead": 1.7, "cotton": 4.0, "silver": 6.3, "paint": 2.9, "paper": 1.4, "plaster": 0.1, 
        "general plastic": 3.3, "polyethylene": 2.5, "ABS": 3.8, "nylon": 7.9, "polycarbonate": 7.6,
        "polypropylene": 3.4, "polystyrene": 3.4, "polyurethane": 4.8, "PVC": 3.1, "Rubber": 2.9, 
        "steel": 2.8, "rebar": 2.0, "general stone": 0.1, "general timber": 0.5, "tin": 3.0,
        "titanium": 25.0}

materials = ee_dict.keys()

print(materials)