const ratings = {
    hotel_a: 2.8};

  
  // total number of stars
  const starTotal = 5;
  
  for (const rating in ratings) {
    const starPercentage = (ratings[rating] / starTotal) * 100;
    const starPercentageRounded = `${Math.round(starPercentage / 10) * 10}%`;
    document.querySelector(`tr.${rating} div.stars-inner`).style.width = 50;
  }
  